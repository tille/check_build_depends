#!/bin/sh

# get a list of all packages and their Build-Depends(-Indep) (one per row)
# if there is a missing Build-Depends for a certain architecture

# ignore whitelist of packages that are expected to be available in any case
BDWHITELIST=" 'cdbs'
             ,'d-shlibs'
             ,'debhelper'
             ,'dpkg-dev'
             ,'libc6-dev'
             ,'perl'
             ,'python'
             ,'python-all'
             ,'python-all-dev'
             ,'python3'
             ,'python3-all'
             ,'python3-all-dev'
             ,'quilt'
             ,'rename'
            "
SRCWHITELIST=" 'debian-installer'
             "

usage() {
cat >/dev/stderr <<EOT
Usage: $0 [option] <arch>
      -m forcing public mirror over local one
      -o print list of soure packages only (without duplicates)
      -h this help screen
      -d verify whether there are false positives

Description:
  Query UDD for packages that can not build on architecture <arch> due to
  missig Build-Depends on that specific <arch>.  The default output are
  two columns listing the source and missing build-depends.  If there are
  more than one missing build-depends for a source package these are listed
  in two rows.  For de-duplication use -o option.
EOT
}

PORT="-p 5452"

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql $PORT -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

# Check UDD connection
if ! psql $PORT $SERVICE -c "" 2>/dev/null ; then
    echo "No local UDD found, use publich mirror."
    PORT="--port=5432"
    export PGPASSWORD="public-udd-mirror"
    SERVICE="--host=public-udd-mirror.xvm.mit.edu --username=public-udd-mirror udd"
fi

EXT=txt
xSOURCELISTONLYSTART="SELECT DISTINCT source FROM ("
xSOURCELISTONLYEND=") tmp"

while getopts "dhmo" o; do
    case "${o}" in
        h)
            usage
            exit 0
            ;;
        m)
           PORT="--port=5432"
           export PGPASSWORD="public-udd-mirror"
           SERVICE="--host=public-udd-mirror.xvm.mit.edu --username=public-udd-mirror udd"
           ;;
        d)
           HEADLINE=-t
           DEBUGSTART="SELECT * FROM packages WHERE release = 'sid' AND architecture = '${ARCH}' AND source IN ("
           DEBUGEND=")"
           SOURCELISTONLYSTART=$xSOURCELISTONLYSTART
           SOURCELISTONLYEND=$xSOURCELISTONLYEND
           ;;
        o)
           HEADLINE=-t
           SOURCELISTONLYSTART=$xSOURCELISTONLYSTART
           SOURCELISTONLYEND="$xSOURCELISTONLYEND ORDER BY source"
           ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [ $# -ne 1 ] ; then
  echo "Missing architecture!"
  echo ""
  usage
  exit 1
fi

ARCH=$1

check=`psql $PORT $SERVICE  -t -c "SELECT DISTINCT architecture FROM packages WHERE architecture != 'all' and architecture = '${ARCH}';"`

if [ "$check" = "" ] ; then
  echo "Architecture $ARCH is not known to UDD"
  usage
  exit 1
fi


psql $HEADLINE --quiet $PORT $SERVICE <<EOT
CREATE TEMPORARY TABLE package_arch AS
  SELECT DISTINCT * FROM (
    SELECT DISTINCT package, architecture FROM packages WHERE release = 'sid' AND architecture in ('all', '${ARCH}')
    UNION
    SELECT DISTINCT regexp_split_to_table(provides, E',') AS package, architecture FROM packages WHERE release = 'sid' AND architecture in ('all', '${ARCH}')
  ) tmp
; 

create function pg_temp.check_alternatives(text) returns text as 
\$\$
   SELECT
   CASE WHEN position('|' in \$1) > 0 THEN
             (SELECT bdalternative FROM ( SELECT regexp_split_to_table(\$1, '\|') AS bdalternative ) bda
              LEFT OUTER JOIN package_arch bd ON bda.bdalternative = bd.package
              LIMIT 1
             )
        ELSE \$1 END
\$\$ language sql;

-- create a temporary table with packages from those sources that can not be built
-- this is needed for a second pass evaluation of the package pool since there might
-- be arch:all packages which can not be build and are not be available for other
-- packages as Build-Depends
CREATE TEMPORARY TABLE second_pass AS
  SELECT package, NULL::text AS architecture FROM ( -- src
    SELECT DISTINCT source FROM ( -- tmp3
      SELECT source, build_depends_single, package, architecture FROM ( -- s
        SELECT source, pg_temp.check_alternatives(regexp_replace(regexp_split_to_table(build_depends, E','),' ','','g')) AS build_depends_single FROM ( -- tmp2
          SELECT source, 
            CASE WHEN build_depends_indep is NULL THEN build_depends ELSE build_depends || ',' || build_depends_indep END AS build_depends, version FROM ( -- tmp
              SELECT source,
                regexp_replace(regexp_replace(regexp_replace(regexp_replace(build_depends, ' +\([^)]+\)','','g'), ' +<[^>]+>','','g'), ' +\[[^\]]+\]','','g'), ' *:[aeinty64]+','','g') AS build_depends,
                regexp_replace(regexp_replace(regexp_replace(regexp_replace(build_depends_indep, ' +\([^)]+\)','','g'), ' +<[^>]+>','','g'), ' +\[[^\]]+\]','','g'), ' *:[aeinty64]+','','g') AS build_depends_indep,
                version,
                ROW_NUMBER () OVER (PARTITION BY source ORDER BY version DESC) AS row -- fetch only highest version number
                FROM sources WHERE release = 'sid'
                               AND source NOT LIKE 'gcc-%' AND source NOT IN ($SRCWHITELIST)
              ) AS tmp WHERE row = 1
          ) tmp2
        ) s
        LEFT OUTER JOIN package_arch bd ON s.build_depends_single = bd.package
        WHERE build_depends_single NOT IN ($BDWHITELIST) and build_depends_single NOT LIKE 'dh-%'
      ) tmp3 WHERE package IS NULL OR architecture IS NULL
    ) src
    JOIN (SELECT DISTINCT source, package FROM packages WHERE release = 'sid' AND architecture in ('all', '${ARCH}')) sp ON sp.source = src.source
;

CREATE TEMPORARY TABLE package_arch_second_pass AS
  SELECT * FROM second_pass
  UNION
  SELECT * FROM package_arch WHERE package NOT IN (SELECT package FROM second_pass)
;

create function pg_temp.check_alternatives_second_pass(text) returns text as 
\$\$
   SELECT
   CASE WHEN position('|' in \$1) > 0 THEN
             (SELECT bdalternative FROM ( SELECT regexp_split_to_table(\$1, '\|') AS bdalternative ) bda
              LEFT OUTER JOIN package_arch_second_pass bd ON bda.bdalternative = bd.package
              LIMIT 1
             )
        ELSE \$1 END
\$\$ language sql;

$DEBUGSTART
$SOURCELISTONLYSTART
SELECT source, build_depends_single AS "Missing Build-Depends on architecture ${ARCH}" FROM (
  SELECT source, build_depends_single, package, architecture FROM (
   SELECT source, pg_temp.check_alternatives_second_pass(regexp_replace(regexp_split_to_table(build_depends, E','),' ','','g')) AS build_depends_single FROM (
    SELECT source, 
          CASE WHEN build_depends_indep is NULL THEN build_depends ELSE build_depends || ',' || build_depends_indep END AS build_depends, version FROM (
      SELECT source,
            regexp_replace(regexp_replace(regexp_replace(regexp_replace(build_depends, ' +\([^)]+\)','','g'), ' +<[^>]+>','','g'), ' +\[[^\]]+\]','','g'), ' *:[aeinty64]+','','g') AS build_depends,
            regexp_replace(regexp_replace(regexp_replace(regexp_replace(build_depends_indep, ' +\([^)]+\)','','g'), ' +<[^>]+>','','g'), ' +\[[^\]]+\]','','g'), ' *:[aeinty64]+','','g') AS build_depends_indep,
            version,
            ROW_NUMBER () OVER (PARTITION BY source ORDER BY version DESC) AS row -- fetch only highest version number
            FROM sources WHERE release = 'sid'
                           AND source NOT LIKE 'gcc-%' AND source NOT IN ($SRCWHITELIST)
      ) AS tmp WHERE row = 1
     ) tmp2
   ) s
   LEFT OUTER JOIN package_arch_second_pass bd ON s.build_depends_single = bd.package
   WHERE build_depends_single NOT IN ($BDWHITELIST) and build_depends_single NOT LIKE 'dh-%'
 ) tmp WHERE package IS NULL OR architecture IS NULL
$SOURCELISTONLYEND
$DEBUGEND
;
EOT
